#!/bin/bash

####
#
# Before you can use this, you have to setup:
# * install docker and docker-compose
# * aws-cli like described:
#   * https://docs.aws.amazon.com/cli/latest/userguide/installing.html
#   * https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html 
#
###



dataset=${1:-amazon_reviews_multilingual_DE_v1_00.tsv.gz}

echo "create database"
echo "create database IF NOT EXISTS reviews CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;use reviews;" > dump.sql

echo "create table"
createTableSTMT="DROP TABLE IF EXISTS reviews; CREATE TABLE reviews (
  marketplace varchar(2),
  customer_id varchar(10),
  review_id varchar(20),
  product_id varchar(20),

  product_parent varchar(255),
  product_title varchar(255),
  product_category varchar(255),

  star_rating INT,
  helpful_votes BIGINT,
  total_votes BIGINT,
  vine varchar(1),
  verified_purchase varchar(1),
  review_headline MEDIUMTEXT,
  review_body LONGTEXT,
  review_date Date,
  PRIMARY KEY (review_id)
);"

echo $createTableSTMT >> dump.sql
echo -e "\n" >> dump.sql

echo "BTW: Don't worry about the errors while importing. I didn't put much effort into the importing as this is only for demonstration purposes of huge datasets. PRs/MRs are welcome ;-)"

aws s3 cp s3://amazon-reviews-pds/tsv/${dataset} - | gzip -d  | grep -v "^marketplace"| sed 's/"/\\"/g' | sed 's/\t/", "/g' | sed 's/\(.*\)/INSERT INTO reviews VALUES("\1");/g' >> dump.sql
