Bigdata Integration Environment Example
=======================================


This is the example project used for the [innomeet.cologne](http://innomeet.cologne)/* presentation will be linked here*/ talk.
This example will setup:

1. [Install minikube, to have a running kubernetes setup on the local machine](#user-content-install-minikube)
1. [Rook, which will take care, that you have an deduplicated elastic block storage available.](user-content-install-rook)
1. [a golden mysql volume with amazon product reviews](user-content-golden-mysql-volume)

This will be used by a two shell scripts

1. which creates new databases
1. which deletes old databases


These concept could be used togehter for example with [Gitlabs review apps](https://docs.gitlab.com/ee/ci/review_apps/)

Benefits are:

* new Environments only costs CPU usage and Memory and blocks which were written during testing.
  * having a real microservice infratstructure with autoscaling these costs could be further reduced
  * by reusing pods with the same version you can eliminate this overhead completely
* you can spawn new environments just in time (so you don't have to have several integration environment running, when you don't need them)


Install minikube
----------------

Installing miniube is straight forward reading the documention: https://kubernetes.io/docs/tasks/tools/install-minikube/


Install rook
--------------

Rook is an awesome peace of software combining new modern way of managing infrastructure parts with a valid solid technology of raw block cloud storage (ceph).
I will use in this example the installation via CRD Files instead of Helm charts, just to keep the complexity of "new" technologies as small as possible.

If you prefer to use the helm chart or getting your own experiences, please follow the documentation: https://rook.io/docs/rook/v0.7/quickstart.html
But be careful to set the data dir of rook to `/data/rook` according to: https://github.com/rook/rook/issues/1192#issuecomment-341947005


### Operator

`kubectl apply -f rook/operator.yaml`

This will create everything related to the orchestrator, as rights, a service account user and the operator deployment itslef.
The operator will now manage "Virtual Ceph" Ressource Definitions and make sure they will be provisioned and stay healthy (at least for trivial outages and failures).

To check if everything worked, check if the operator is running with (this can take a few minutes):

`kubectl -n rook-system get pods`

If the pod is not running, we have to troubleshoot the pod with:

`kubectl -n rook-system describe pods`


### Cluster

This CRD (which comes with the rook operator) will describe a Ceph Cluster, the rook-operator will create. It basically only creates a namespace and then on all nodes installs ceph.

`kubectl apply -f rook/cluster.yaml`


This will create first ceph monitors and then mgr and osds. If everything worked as it should, you should see something like running `kubectl -n rook get all`:

```
NAME                             READY     STATUS    RESTARTS   AGE
rook-api-8677588559-xs2f8        1/1       Running   0          2m
rook-ceph-mgr0-79f59b566-c67qt   1/1       Running   0          2m
rook-ceph-mon0-69gd5             1/1       Running   0          2m
rook-ceph-mon1-jq5hv             1/1       Running   0          2m
rook-ceph-mon2-rvrcv             1/1       Running   0          2m
rook-ceph-osd-szf54              1/1       Running   0          2m

NAME             TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
rook-api         ClusterIP   10.106.226.247   <none>        8124/TCP   2m
rook-ceph-mgr    ClusterIP   10.97.62.221     <none>        9283/TCP   2m
rook-ceph-mon0   ClusterIP   10.109.94.147    <none>        6790/TCP   2m
rook-ceph-mon1   ClusterIP   10.106.163.49    <none>        6790/TCP   2m
rook-ceph-mon2   ClusterIP   10.102.21.246    <none>        6790/TCP   2m

NAME            DESIRED   CURRENT   READY     UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
rook-ceph-osd   1         1         1         1            1           <none>          2m

NAME             DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
rook-api         1         1         1            1           2m
rook-ceph-mgr0   1         1         1            1           2m

NAME                       DESIRED   CURRENT   READY     AGE
rook-api-8677588559        1         1         1         2m
rook-ceph-mgr0-79f59b566   1         1         1         2m
rook-ceph-mon0             1         1         1         2m
rook-ceph-mon1             1         1         1         2m
rook-ceph-mon2             1         1         1         2m
```

If not you have to describe pods and replicasets and daemonsets. If nothing appears, you may have to delete your rook operator as he gets stalled from time to time and does not watch the cluster CRD anymore.


### Rook Tools

One of the most important container, which I learned too late is the tools container. We will also use this container to do our integration magic even though we could create an rbd user, install rbd on our ci system (or use an rbd ready docker base image) and run those commands directly threw rbd.

But as I want to keep this example simple, I will use the existing container.

`kubectl apply -f rook/tools.yaml`

This will create a new container in our rook namespace. To check if everything worked we can run:

`kubectl -n rook describe pod rook-tools`

Now we can check, if rook did a good job with:

```
kubectl -n rook exec -it rook-tools -- ceph -w
```
which should print sth like:

```
  cluster:
    id:     c03759de-3efa-4c48-a428-25930d02b405
    health: HEALTH_OK
 
  services:
    mon: 3 daemons, quorum rook-ceph-mon1,rook-ceph-mon2,rook-ceph-mon0
    mgr: rook-ceph-mgr0(active)
    osd: 1 osds: 1 up, 1 in
 
  data:
    pools:   0 pools, 0 pgs
    objects: 0 objects, 0 bytes
    usage:   4454 MB used, 12037 MB / 16492 MB avail
    pgs:
```

So we don't have any pgs now, as we don't have any pool, but we have 3 mons in quorum and one osd node, which is exactly what we need.

### Storage Class

Now we have to deploy the storage class together with the rook-pool. Therefore run:

`kubectl apply -f rook/storageclass.yaml`

To check if everything worked we can run: 
* `kubectl get storageclass` which will show us our rook-block storageclass
* `kubectl -n rook exec -it rook-tools -- ceph -w` which will now show us that there is one pool and 100 placement groups which are hopefully:
  ```
    data:
      pools:   1 pools, 100 pgs
      objects: 0 objects, 0 bytes
      usage:   4456 MB used, 12036 MB / 16492 MB avail
      pgs:     100 active+clean
  ```

As I said, the rook-tools container is very helpful troubleshooting the ceph cluster. E.g when one data node (osd) would fail and you would have more than one, you could see this also in unhealthy pgs. When the node recovers you would also see how many placement groups are left, which are not active+clean.


Golden MySQL volume
---------------------

So now lets create our golden MySQL volume, we will use later to create our Integration Clones.


In a real scenario, I would create a golden image once a day. The most challenging part here is the anonymization part.
In fact we can do this part in four steps:

1. import backup into our MySQL Breeder instance
1. anonymize the database
1. removing old snapshots
1. create a new snapshot

First we have to create our mysql-breeder instane itself:

```
kubectl apply -f mysql/statefulset-mysql.yaml
```

### Anonymization

We will skip the anonymize process in our practical example here.


### Import backup

To get an example dataset, you can use the **import.sh** script. (Docs are in the header of that script)

Importing the backup will do with:
`mysql -h $(minikube ip) -P 30306 -uroot -f -pinnomeet < dump.sql`

### Create Snapshot to be used as Golden Image

Now it's getting a bit messy, but don't be afraid. First we have to find out the name of our rados block image. Therefore we are getting first our Persistent Volume Claim:
`kubectl get pvc -l app=mysql-breeder -o yaml`

To use this in another script we have to filter just the volumeName, which we can do with jsonpath output formater:
`rbdImage=$(kubectl get pvc -l app=mysql-breeder -o jsonpath='{.items[*].spec.volumeName}')`


Next step is to create a Snapshot from this Image, where we are using again rook-tools.
`kubectl -n rook exec -it rook-tools -- rbd snap create replicapool/${rbdImage}@goldenimage`

To be able to clone this image, we have to protect it:
`kubectl -n rook exec -it rook-tools -- rbd snap protect replicapool/${rbdImage}@goldenimage`

create new Environment
----------------------


### Clone Image manually

Now we are able to create a new image
`kubectl -n rook exec -it rook-tools -- rbd clone replicapool/${rbdImage}@goldenimage replicapool/integration-1`

### Create PVs and App manually

We simply have to use this rbd image. So we have to define it in our Persistent Volume like:

```
apiVersion: v1
kind: PersistentVolume
metadata:
  annotations:
    pv.kubernetes.io/provisioned-by: rook.io/block
  name: pvc-int1-mysql
  labels:
    environment: integration-1
    app: mysql
spec:
  accessModes:
  - ReadWriteOnce
  capacity:
    storage: 1Gi
  flexVolume:
    driver: rook.io/rook
    options:
      image: integration-1
      pool: replicapool
      storageClass: rook-block
  persistentVolumeReclaimPolicy: Delete
  storageClassName: rook-block
```

To create a StatefulSet using this Volume we have to set the PersistentVolumeClaimTemplate with the same spec and name as this PV (If you need a hint, just look at the shell script).


### Use automated Scripts

Now we just have to do some bash magic and we are good to deploy as many environemnts as we want to.
Therefore take a look at `deploy-environment.sh`. You then can use the commit sha as environment name.
Also you would have to create an Ingress for your app, which can be `<commitsha>.integration.yourdomain.tld`

To stop the environment you just have to execute `stop-environment.sh`


Testing
--------------


Now everything is setup we can use our integration databases. Therefore we just have to create an environment with:
`./deploy-environment.sh <name>`

Get Ports with;
`kubectl get services`

and connect with any mysql client like:
`mysql -h $(minikube ip) -P <port> -uroot -pinnomeet`

To stop the environment we just have to run:
`./stop-environment.sh <name>`
